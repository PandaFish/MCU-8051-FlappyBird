# MEME PROJECT

# MCU-8051-FlappyBird
 The old FlappyBird game from 2013 but it's made in Assembly on the MCU 8051

# (FappyBirb)

Development:
MCU 8051 IDE used ( https://sourceforge.net/projects/mcu8051ide/files/mcu8051ide/1.4.9/ )

Used Virtual Hardware:
Simple Keypad
8x8 LED Matrix

Run:
Load the configs simpleKeypad.vhc and ledMatrix.vhc in the corresponding VH.
You may then start & run the program. The keys on the simpleKeypad are used for flight control.
