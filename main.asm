org 000H
jmp main 

org 003H
	ljmp birberuptup 

org 013H
	ljmp birberuptdown

main:
	; sets interrupts
	setb EA 
	setb EX0	
	setb IT0	
	setb EX1
	setb IT1
	
	; birbstop x-axis
	mov R3, #10111111b
	; birbstop y-axis
	mov R4, #00011000b

	; birb x-axis
	mov R5, #11111110b
	; birb y-axis
	mov R6, #11111110b

	; shows birb and birbstop in matrix
	jmp showbirbstuff
	
birbpause:
	; sets a pause to match the matrix fade out
	djnz R1,$
	
	jmp movebirbstuff

; ISR 1
birberuptdown: ; birb goes down
	; writes P1 in A 
	mov A,R6
	; rotates bits in the byte on step to the left
	rl A
	; writes rotated byte back in R6
	mov R6,A
	; marks the end of the interrupt
	clr IE0
	RETI

; ISR 2
birberuptup: ; birb goes up
	; writes P1 in A 
	mov A,R6
	; rotates bits in the byte on step to the left
	rr A
	rr A
	; writes rotated byte back in R6
	mov R6,A
	; marks the end of the interrupt
	clr IE1
	RETI

movebirbstuff:
	; writes P1 in A
	mov A,R6
	; rotates bits in the byte on step to the left
	rl A
	; writes rotated byte back in R6
	mov R6,A
	; writes P1 in A for later multiplication
	mov A,R3
	; rotates bits in the byte one step to the right
	rr A
	; writes A back in R3
	mov R3,A
	
	jmp showbirbstuff
	
showbirbstuff:
	mov P0, R3
	mov P1, R4

	; Collisiondetection
	jnb P0.0, birbcolide
	
	mov P0, #11111111b
	mov P1, #11111111b

	mov P0, R5
	mov P1, R6

	mov P0, #11111111b
	mov P1, #11111111b
	
	;length of the pause
	mov R1,#20H 
	jmp birbpause

showbirbwall:
	mov P0, #11111111b
	mov P1, #11111111b

	mov P0, R5
	mov P1, R6

	mov P0, #11111111b
	mov P1, #11111111b
	
	;length of the pause
	mov R1,#20H 
	jmp birbpause

birbcolide:
	cjne R6, #11110111b, birbcolide2
	jmp showbirbwall


birbcolide2:
	cjne R6, #11101111b, birbdead1
	jmp showbirbwall

birbdead1:
	mov P0, #01100110b
	mov P1, #01100110b

	jmp birbdead2

birbdead2:
	mov P0, #10011001b
	mov P1, #10011001b

	jmp birbdead1

END


